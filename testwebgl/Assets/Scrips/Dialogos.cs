using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ControlUI;
using UnityEngine.UI;
using TMPro;
using System;

public class Dialogos : MonoBehaviour
{
       
    //Archivo Json en .txt
    public TextAsset textJson;
    private string jsonString;
    public DataDialogList dialogos = new DataDialogList();

    private int paso = 0;
    public TMP_Text texto;
    public GameObject GO_Pj;
    public GameObject GO_Npc;
    public GameObject GO_Noticia;
    public GameObject GO_PreguntaQuiz;
    public GameObject GO_PreguntaNoticia;
    public GameObject GO_PreguntaPerfil;
    public GameObject GO_Recompensas;
    public GameObject recuadroDialogos;
    public GameObject BotonContinuar;
    public GameObject DifusoTransicion;

    public GameObject GO_resultados;
    private Resultados src_resultados;
   
    private PreguntaQuiz src_preguntaQuiz;
    private PreguntaNoticia src_preguntaNoticia;
    private PreguntaPerfil src_preguntaPerfil;
    private Image sprite_Npc;
   

    public Button botonNoticia;
    public Sprite[] NPCsprite;
    public string[] url_Noticias;
    public Cuerpo cuerpo;

    //Evento
    public static Action<bool> OnPlaying;

    private void Start()
    {
        sprite_Npc = GO_Npc.GetComponent<Image>();
        src_preguntaQuiz = GO_PreguntaQuiz.GetComponent<PreguntaQuiz>();
        src_preguntaNoticia = GO_PreguntaNoticia.GetComponent<PreguntaNoticia>();
        src_preguntaPerfil = GO_PreguntaPerfil.GetComponent<PreguntaPerfil>();
        src_resultados = GO_resultados.GetComponent<Resultados>();
        LeerJson();
    }
    public void IniciarGuion()
    {
        OnPlaying?.Invoke(false);
        sprite_Npc = GO_Npc.GetComponent<Image>();
        src_preguntaQuiz = GO_PreguntaQuiz.GetComponent<PreguntaQuiz>();
        src_preguntaNoticia = GO_PreguntaNoticia.GetComponent<PreguntaNoticia>();
        src_preguntaPerfil = GO_PreguntaPerfil.GetComponent<PreguntaPerfil>();
        src_resultados = GO_resultados.GetComponent<Resultados>();
        LeerJson();
        Paso();
    }
    public void Paso()
    {
        sprite_Npc = GO_Npc.GetComponent<Image>();
        src_preguntaQuiz = GO_PreguntaQuiz.GetComponent<PreguntaQuiz>();
        src_preguntaNoticia = GO_PreguntaNoticia.GetComponent<PreguntaNoticia>();
        src_preguntaPerfil = GO_PreguntaPerfil.GetComponent<PreguntaPerfil>();
        src_resultados = GO_resultados.GetComponent<Resultados>();
        LeerJson();
        //Debug.Log(paso);
        Debug.Log(dialogos.guion[paso].id);

        switch (dialogos.guion[paso].accion)
        {
            case "null":
                GO_PreguntaQuiz.SetActive(false);
                GO_PreguntaNoticia.SetActive(false);
                GO_PreguntaPerfil.SetActive(false);
                recuadroDialogos.SetActive(true);
                DifusoTransicion.SetActive(true);
                PersonajeHablando(dialogos.guion[paso].personaje);
                texto.text = dialogos.guion[paso].texto;
                GO_Noticia.SetActive(false);
                paso++;
                break;

            case "Noticia":
                GO_PreguntaQuiz.SetActive(false);
                GO_PreguntaNoticia.SetActive(false);
                GO_PreguntaPerfil.SetActive(false);
                recuadroDialogos.SetActive(false);
                DifusoTransicion.SetActive(false);
                PersonajeHablando(dialogos.guion[paso].personaje);
                cuerpo.SeleccionarPlayer();
                GO_Noticia.SetActive(true);
                botonNoticia.onClick.AddListener(OpenURL);
                paso++;
                break;

            case "PreguntaQuiz":
                GO_Npc.SetActive(false);
                GO_Pj.SetActive(false);
                GO_Noticia.SetActive(false);
                recuadroDialogos.SetActive(false);
                BotonContinuar.SetActive(false);
                DifusoTransicion.SetActive(true);
                GO_PreguntaQuiz.SetActive(true);
                GO_PreguntaNoticia.SetActive(false);
                GO_PreguntaPerfil.SetActive(false);
                //Debug.Log(dialogos.guion[paso].numero);
                src_preguntaQuiz.MostrarPregunta(dialogos.guion[paso].numero);
                paso++;
                break;

            case "PreguntaNoticia":
                GO_Npc.SetActive(false);
                GO_Pj.SetActive(false);
                GO_Noticia.SetActive(false);
                recuadroDialogos.SetActive(false);
                BotonContinuar.SetActive(false);
                DifusoTransicion.SetActive(true);
                GO_PreguntaQuiz.SetActive(false);
                GO_PreguntaNoticia.SetActive(true);
                GO_PreguntaPerfil.SetActive(false);
                //Debug.Log(dialogos.guion[paso].numero);
                src_preguntaNoticia.MostrarPregunta(dialogos.guion[paso].numero);
                paso++;
                break;

            case "PreguntaPerfil":
                GO_Npc.SetActive(false);
                GO_Pj.SetActive(false);
                GO_Noticia.SetActive(false);
                recuadroDialogos.SetActive(false);
                BotonContinuar.SetActive(false);
                DifusoTransicion.SetActive(true);
                GO_PreguntaQuiz.SetActive(false);
                GO_PreguntaNoticia.SetActive(false);
                GO_PreguntaPerfil.SetActive(true);
                //Debug.Log(dialogos.guion[paso].numero);
                src_preguntaPerfil.MostrarPregunta(dialogos.guion[paso].numero);
                paso++;
                break;

            case "Codigo":
                int val = dialogos.guion[paso].numero;
                GO_PreguntaQuiz.SetActive(false);
                GO_PreguntaNoticia.SetActive(false);
                GO_PreguntaPerfil.SetActive(false);
                recuadroDialogos.SetActive(false);
                DifusoTransicion.SetActive(false);
                PersonajeHablando(dialogos.guion[paso].personaje);
                texto.text = dialogos.guion[paso].texto;
                GO_Noticia.SetActive(false);
                paso = 0;
                
                if (val <= 4)
                {
                    GO_Recompensas.SetActive(true);
                    GO_Recompensas.GetComponent<Recompensas>().RecompenzaLinea();
                }
                else
                {
                    GO_Recompensas.GetComponent<Recompensas>().DarRecompensas();
                    GO_Recompensas.GetComponent<Recompensas>().RegresarMenu();
                }
                break;

            case "Resultados":
                src_resultados.CalcularResultados();
                GO_resultados.SetActive(true);
                break;
        }      
    }
    public void DialogoTransicion()
    {

        recuadroDialogos.SetActive(true);
        PersonajeHablando(dialogos.guion[paso].personaje);
        texto.text = dialogos.guion[paso].texto;
        paso++;
    }

    public void PersonajeHablando(string _personaje)
    {
        //Debug.Log(_personaje);
        if (_personaje == Enum_Personajes.Kendry.ToString())
        {
            sprite_Npc.sprite = NPCsprite[0];
        }
        if (_personaje == Enum_Personajes.Megumi.ToString())
        {
            sprite_Npc.sprite = NPCsprite[1];
        }
        if (_personaje == Enum_Personajes.Tibomilo.ToString())
        {
            sprite_Npc.sprite = NPCsprite[2];
        }
        if (_personaje == Enum_Personajes.Yinet.ToString())
        {
            sprite_Npc.sprite = NPCsprite[3];
        }
        if (_personaje == Enum_Personajes.Pj.ToString())
        {
            GO_Npc.SetActive(false);
            GO_Pj.SetActive(true);
        }
        else
        { 
            GO_Npc.SetActive(true);
            GO_Pj.SetActive(false);
        }   
    }

    public void OpenURL()
    {
        Application.OpenURL(url_Noticias[dialogos.guion[paso].numero]);
    }

    [System.Serializable]
    public class DataDialog
    {
        public int id;
        public string personaje;
        public string texto;
        public string accion;
        public int numero;
    }
    [System.Serializable]
    public class DataDialogList
    {
        public DataDialog[] guion;
    }

    public void LeerJson()
    {
        dialogos = JsonUtility.FromJson<DataDialogList>(textJson.text);
    }
}
