using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playercustom : MonoBehaviour
{   

    public Color[] ColoresCuerpo;
    private PlayerController playerController;

    #region IDs
        public int id_Cuerpo = 0; 
        public int id_Camiza;
        public int id_Cabeza;
        public int id_Pantalones;
        public int id_Accesorios;
        public int id_Zapatos;
    #endregion

    #region Imagenes
        public Image Cabeza;
        public Image Camiza;
        public Image Pantalon;
        public Image Accesorio;
        public Image Zapato;
        public Image Cuerpo;
        public Image Cuellos;
        public Image Brazos;    
    #endregion

    #region  Sprites
        public Sprite[] Cabezas;
        public Sprite[] Camizas;
        public Sprite[] Pantalones;
        public Sprite[] Accesorios;
        public Sprite[] Zapatos;
    #endregion

    
    void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        //IniciarID();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void IniciarID()
    {
        id_Accesorios = 0;
        id_Cabeza = 0;
        id_Camiza = 0;
        id_Cuerpo = 0;
        id_Pantalones = 0;
        id_Zapatos = 0;
        //UpdatePersonaje();
    }

    public void UpdatePersonaje(Enum_Partes _parte)
    {
        playerController = FindObjectOfType<PlayerController>();
        switch (_parte)
        {
            case Enum_Partes.Cabeza:
                Cabeza.gameObject.SetActive(true);
                Cabeza.sprite = Cabezas[id_Cabeza];
                playerController.SelecionarSprite(Enum_Partes.Cabeza, Cabeza.sprite);
                break;

            case Enum_Partes.Camisa:
                Camiza.gameObject.SetActive(true);
                Camiza.sprite = Camizas[id_Camiza];
                playerController.SelecionarSprite(Enum_Partes.Camisa, Camiza.sprite);
                break;

            case Enum_Partes.Pantalon:
                Pantalon.gameObject.SetActive(true);
                Pantalon.sprite = Pantalones[id_Pantalones];
                playerController.SelecionarSprite(Enum_Partes.Pantalon, Pantalon.sprite);
                break;

            case Enum_Partes.Accesorio:
                Accesorio.gameObject.SetActive(true);
                Accesorio.sprite = Accesorios[id_Accesorios];
                playerController.SelecionarSprite(Enum_Partes.Accesorio, Accesorio.sprite);
                break;

            case Enum_Partes.Zapatos:
                Zapato.gameObject.SetActive(true);
                Zapato.sprite = Zapatos[id_Zapatos];
                playerController.SelecionarSprite(Enum_Partes.Zapatos, Zapato.sprite);
                break;

            case Enum_Partes.Cuerpo:
                Brazos.color = ColoresCuerpo[id_Cuerpo];
                Cuellos.color = ColoresCuerpo[id_Cuerpo];
                Cuerpo.color = ColoresCuerpo[id_Cuerpo];
                playerController.SelecionarColor(ColoresCuerpo[id_Cuerpo]);
                break;
        }
        Brazos.color = ColoresCuerpo[id_Cuerpo];
        Cuellos.color = ColoresCuerpo[id_Cuerpo];
        Cuerpo.color = ColoresCuerpo[id_Cuerpo];
        playerController.SelecionarColor(ColoresCuerpo[id_Cuerpo]);
    }

    public void UpdateCuerpo(int id)
    {
        id_Cuerpo = id;
        UpdatePersonaje(Enum_Partes.Cuerpo);
    }    
}
