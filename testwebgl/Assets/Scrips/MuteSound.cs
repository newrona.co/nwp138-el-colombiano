using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class MuteSound : MonoBehaviour
{
    public AudioSource audioController;
    public AudioMixer  audioMixer;
    public Image boton;
    public Sprite[] imagenes;
    public Slider sliderVolumen;


    public void SetVolumen()
    {
        audioMixer.SetFloat("Volumen", sliderVolumen.value);
    }
    public void QuitarAudio()
    {
        float volumen;
        audioMixer.GetFloat("Volumen", out volumen);
        if ( volumen == 0)
        {
            audioMixer.GetFloat("Volumen", out volumen);
            boton.sprite = imagenes[0];
        }
        else 
        {
            audioController.mute = true;
            boton.sprite = imagenes[1];
        } 
    }
}
