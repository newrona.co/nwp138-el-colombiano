using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using ControlUI;
using UnityEngine.Video;
using UnityEngine.Networking;

public class Inicio : MonoBehaviour
{
    public TMP_Text tmp_texto;
    public PlayerController pj;
    public GameController gameController;

    public VideoPlayer videoEscenario;
    public Animator animCarga;
    public string Url;


    public string texto;


    private void UpdateUrl()
    {
        animCarga.SetTrigger("fadeIn");
        videoEscenario.url = System.IO.Path.Combine(Application.streamingAssetsPath, Url);
        videoEscenario.prepareCompleted += PrepareTransicion;
        videoEscenario.Prepare();
    }

    void PrepareTransicion(VideoPlayer source)
    {
        videoEscenario.Play();
        animCarga.SetTrigger("fadeOut");
    }

    public void PonerNombre()
    {
        tmp_texto.text = pj.Nombre + texto;
        UpdateUrl();
    }

    public void NextMenu()
    { 
        gameController.UpdateNivel(Enum_Niveles.Menu);
    }
}
