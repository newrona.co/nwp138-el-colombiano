using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ControlUI
{
    public class Recompensas : MonoBehaviour
    {
        public int lineaCodigo;
        public bool prendas;
        public int[] ubicacion;
        public int ubicacionJugada;
        public int nota;
        public bool DarNota;

        public GameObject dialogos;
        public GameObject boton_fin;

        private GameController gameControler;

        public Sprite[] iconos;
        public Image icono;
        public TMP_Text texto;
        public string[] Lineas;

        private bool prendaEntregada = false;
        private bool notaEntregada = false;

        public Enum_Partes[] parte;
        public int[] id_prenda;
        public Cuerpo cuerpo;

   
         
        public void RecompenzaPrendas()
        {
            gameControler = FindObjectOfType<GameController>();
            boton_fin.SetActive(false);
            cuerpo.SeleccionarPlayer();
            if (EscenarioJugado())
            {
                icono.sprite = iconos[2];
                texto.text = "Desbloqueaste prendas para tu avatar. \n�P�rchese la mejor pinta!";
                this.GetComponent<Animator>().SetTrigger("FadeIn");
            }
        }

        public void RecompenzaNota()
        {
            gameControler = FindObjectOfType<GameController>();
            boton_fin.SetActive(false);
            cuerpo.SeleccionarPlayer();
            if (EscenarioJugado())
            {
                icono.sprite = iconos[1];
                texto.text = "Desbloqueaste una nota de prensa. \n�Mucho nerd!";
                this.GetComponent<Animator>().SetTrigger("FadeIn");
            }
        }

        public void RecompenzaLinea()
        {
            gameControler = FindObjectOfType<GameController>();
            boton_fin.SetActive(true);
            cuerpo.SeleccionarPlayer();
            if (EscenarioJugado())
            {
                notaEntregada = true;
                icono.sprite = iconos[0];
                texto.text = "Desbloqueaste una l�nea del C�digo Parche. \n�Mero crac!";
                gameControler.LineasCodigo[lineaCodigo] = true;
                DarRecompensas();
                this.GetComponent<Animator>().SetTrigger("FadeIn");
            }
        }

     

        private void OnEnable()
        {
            /*
            cuerpo.SeleccionarPlayer();
            if (EscenarioJugado())
            {
                //Escenario Jugado
                if (ubicacion != -1)
                { gameControler.EscenariosDesbloqueados[ubicacion] = true; }

                if (lineaCodigo != -1)
                {
                    icono.sprite = iconos[0];
                    texto.text = "Desbloqueaste una nueva Linea del CodigoParche";
                    gameControler.LineasCodigo[lineaCodigo] = true;
                    this.GetComponent<Animator>().SetTrigger("FadeIn");
                }
                else
                {
                    if (DarNota)
                    {
                        icono.sprite = iconos[1];
                        notaEntregada = true;
                        texto.text = "Desbloqueaste una nueva Nota";
                        gameControler.Notas[nota] = true;
                        this.GetComponent<Animator>().SetTrigger("FadeIn");
                    }
                    else
                    {
                        icono.sprite = iconos[2];
                        DarRecompensas();
                        texto.text = "Desbloqueaste nuevas prendas";
                        this.GetComponent<Animator>().SetTrigger("FadeIn");
                    }
                }
            }
            else {
                RegresarMenu();
            }
             */
        }

        public void SiguienteRecompenza()
        {
            this.GetComponent<Animator>().SetTrigger("FadeOut");
            if (!prendaEntregada)
            {
                icono.sprite = iconos[2];
                DarRecompensas();
                texto.text = "Desbloqueaste nuevas prendas";
                this.GetComponent<Animator>().SetTrigger("FadeIn");
                
            }
            else {
                if (!notaEntregada)
                {
                    icono.sprite = iconos[1];
                    notaEntregada = true;
                    texto.text = "Desbloqueaste una nueva Nota";
                    gameControler.Notas[nota] = true;
                    this.GetComponent<Animator>().SetTrigger("FadeIn");
                }
                else {
                    RegresarMenu();
                }
            }
        }

        public void RegresarMenu()
        {
            gameControler = FindObjectOfType<GameController>();
            dialogos.SetActive(false);
            this.gameObject.SetActive(false);
            gameControler.UpdateNivel(Enum_Niveles.Menu);
        }

        public bool EscenarioJugado()
        {
            return !gameControler.EscenariosJugados[ubicacionJugada];
        }

        public void DarRecompensas()
        {
            gameControler.EscenariosJugados[ubicacionJugada] = true;
            int count = 0;
            foreach (var _prenda in parte)
            {
                foreach (var _parte in gameControler.PartesDesbloqueadas.PartesLista)
                {
                    if ((_parte.parte == parte[count].ToString()) && (_parte.id == id_prenda[count]))
                    {
                        _parte.desbloqueado = true;
                    }
                }
                count++;
            }
            //Escenario Jugado
            foreach (int _ubicacion in ubicacion)
            {
                gameControler.EscenariosDesbloqueados[_ubicacion] = true;
            }
            prendaEntregada = true;
            gameControler.Notas[nota] = true;
        }
    }
}
  
