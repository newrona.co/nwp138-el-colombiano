using ControlUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Options : MonoBehaviour
{

    [SerializeField] GameController gameController;
    [SerializeField] GameObject defaultInfoObject;
    [SerializeField] GameObject tutorial;
    [SerializeField] GameObject infoButton;
    [SerializeField] GameObject creditos;
    [SerializeField] Toggle toggle;


    private void OnEnable()
    {
        MenuController.OnMenuOpened+= ToggleOptions;
        Dialogos.OnPlaying += ToggleOptions;
    }

    private void OnDisable()
    {
        MenuController.OnMenuOpened -= ToggleOptions;
        Dialogos.OnPlaying -= ToggleOptions;

    }


    void ToggleOptions(bool state)
    {
        //infoButton.gameObject.SetActive(state);
        toggle.isOn = false;
    }

    private void Awake()
    {
        if( gameController == null ) gameController= FindObjectOfType<GameController>();
    }

    public void ShowInfo()
    {
        foreach (var item in gameController.GO_Escenarios)
        {
            if (item.activeSelf)
            {
                var controller = item.GetComponentInChildren<UbicacionesController>();
                if (controller == null) return;
                controller.popUpEscenario.gameObject.SetActive(true);
                return;
            }
        }
        if(defaultInfoObject!=null)
            defaultInfoObject.SetActive(true);
    }



    public void ShowTutorial()
    {
        tutorial.gameObject.SetActive(true);
    }

    public void ShowCreditos()
    {
        creditos.SetActive(true);
    }

}
