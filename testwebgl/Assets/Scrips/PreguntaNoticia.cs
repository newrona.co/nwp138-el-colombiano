using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PreguntaNoticia : MonoBehaviour
{
    //Archivo Json en .txt
    public TextAsset textJson;
    private string jsonString;

    public int numeroPreguta;

    public TMP_Text text_pregunta;
    public TMP_Text text_respuesta_a;
    public TMP_Text text_respuesta_b;

    public int opcionEscojida = 0;

    public DataPreguntasList listaPreguntas = new DataPreguntasList();

    public GameObject sinRespuesta;
    public GameObject RespuestaCorrecta;
    public GameObject RespuestaIncorrecta;
    public Dialogos dialogos;
    private Image spriteLetra;

    public Button[] botonesOpciones;
    public Sprite spriteBotonesOn;
    public Sprite spriteBotonesOff;

    public PlayerController Pj;
    public Cuerpo cuerpo;

    private void Start()
    {
        LeerJson();
    }

    public void MostrarPregunta(int _numeroPregunta)
    {
        cuerpo.SeleccionarPlayer();
        numeroPreguta = _numeroPregunta;
        LeerJson();
        text_respuesta_a.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionA.texto;
        text_respuesta_b.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionB.texto;

        text_pregunta.text = listaPreguntas.preguntas[numeroPreguta - 1].pregunta;
    }


    public void VerificarRespuesta()
    {
        switch (opcionEscojida)
        {
            case 1:
                if (listaPreguntas.preguntas[numeroPreguta - 1].opcionA.inclinacion == "true")
                {
                    RespuestaCorrecta.SetActive(true);
                }
                else 
                {
                    RespuestaIncorrecta.SetActive(true);
                }
                break;
            
            case 2:
                if (listaPreguntas.preguntas[numeroPreguta - 1].opcionB.inclinacion == "true")
                {
                    RespuestaCorrecta.SetActive(true);
                }
                else
                {
                    RespuestaIncorrecta.SetActive(true);
                }
                break;
        }
    }

    public void ContinuarGuion()
    {
        //EnviarPuntaje();
        dialogos.Paso();
    }
    public void SeleccionarOpcion(int _opcion)
    {
        opcionEscojida = _opcion;
        int count = 0;
        foreach (Button _boton in botonesOpciones)
        {   
            _boton.GetComponent<Image>().sprite = spriteBotonesOff;
            count++;
        }
        botonesOpciones[_opcion - 1].GetComponent<Image>().sprite = spriteBotonesOn;
    }

    public void Responder()
    {
        if (opcionEscojida == 0)
        { sinRespuesta.SetActive(true); }
        else 
        {
            int count = 0;
            foreach (Button _boton in botonesOpciones)
            {
                _boton.GetComponent<Image>().sprite = spriteBotonesOff;
                count++;
            }
            VerificarRespuesta(); 
        }
    }

    public void EnviarPuntaje()
    {
        switch (opcionEscojida)
        {
            case 1:
                Pj.SumarPuntos(listaPreguntas.preguntas[numeroPreguta - 1].opcionA.inclinacion, listaPreguntas.preguntas[numeroPreguta - 1].opcionA.multiplicador);
                break;

            case 2:
                Pj.SumarPuntos(listaPreguntas.preguntas[numeroPreguta - 1].opcionB.inclinacion, listaPreguntas.preguntas[numeroPreguta - 1].opcionB.multiplicador);
                break;

            case 3:
                Pj.SumarPuntos(listaPreguntas.preguntas[numeroPreguta - 1].opcionC.inclinacion, listaPreguntas.preguntas[numeroPreguta - 1].opcionC.multiplicador);
                break;

            case 4:
                Pj.SumarPuntos(listaPreguntas.preguntas[numeroPreguta - 1].opcionD.inclinacion, listaPreguntas.preguntas[numeroPreguta - 1].opcionD.multiplicador);
                break;
        }
    }


    [System.Serializable]
    public class DataRespuetas
    {
        public string texto;
        public string inclinacion;
        public int multiplicador;
    }
   
    [System.Serializable]
    public class DataPreguntas
    {
        public int numero;
        public string pregunta;
        public bool directa;
        public DataRespuetas opcionA;
        public DataRespuetas opcionB;
        public DataRespuetas opcionC;
        public DataRespuetas opcionD;
    }
    [System.Serializable]
    public class DataPreguntasList
    {
        public DataPreguntas[] preguntas;
    }

    void LeerJson()
    {
         listaPreguntas = JsonUtility.FromJson<DataPreguntasList>(textJson.text);
    }
}
