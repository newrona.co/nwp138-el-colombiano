using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ConfirmacionUbicacion : MonoBehaviour
{
    public TMP_Text enacabezado;
    public string[] nombreUbicacion;
    public GameObject Menu;
    public GameObject[] Ubicaciones;
    private int escenario;

    public void SetIndex(int _index)
    {
        escenario = _index;
    }

    private void OnEnable()
    {
        enacabezado.text = nombreUbicacion[escenario];
    }

    public void Ubicacion()
    {
        Ubicaciones[escenario].SetActive(true);
        this.gameObject.SetActive(false);
        Menu.SetActive(false);
    }


    public void Cerrar()
    {
        this.gameObject.SetActive(false);
    }
}
