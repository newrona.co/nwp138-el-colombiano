using UnityEngine;
using UnityEngine.UI;

public class Cuerpo : MonoBehaviour
{
    private PlayerController playerController;

    [Space]
    public Image cuerpo;

    [Space]
    public Image cuello;

    [Space]
    public Image brazos;

    [Space]
    public Image cabeza;

    [Space]
    public Image camisa;

    [Space]
    public Image pantalon;

    [Space]
    public Image zapato;

    [Space]
    public Image accesorio;


    public void SeleccionarPlayer()
    {
            playerController = FindObjectOfType<PlayerController>();

            cuerpo.color = playerController.colorCuerpo;

            cuello.color = playerController.colorCuerpo;

            brazos.color = playerController.colorCuerpo;

        if (playerController.partesSprite.ContainsKey(Enum_Partes.Cabeza))
        {
            cabeza.gameObject.SetActive(true);
            cabeza.sprite = playerController.partesSprite[Enum_Partes.Cabeza];
        }   

        if (playerController.partesSprite.ContainsKey(Enum_Partes.Camisa))
        {
            camisa.gameObject.SetActive(true);
            camisa.sprite = playerController.partesSprite[Enum_Partes.Camisa];
        }

        if (playerController.partesSprite.ContainsKey(Enum_Partes.Pantalon))
        {
            pantalon.gameObject.SetActive(true);
            pantalon.sprite = playerController.partesSprite[Enum_Partes.Pantalon];
        }

        if (playerController.partesSprite.ContainsKey(Enum_Partes.Zapatos))
        {
            zapato.gameObject.SetActive(true);
            zapato.sprite = playerController.partesSprite[Enum_Partes.Zapatos];
        }

        if (playerController.partesSprite.ContainsKey(Enum_Partes.Accesorio))
        {
            accesorio.gameObject.SetActive(true);
            accesorio.sprite = playerController.partesSprite[Enum_Partes.Accesorio];
        }    
    }

}
