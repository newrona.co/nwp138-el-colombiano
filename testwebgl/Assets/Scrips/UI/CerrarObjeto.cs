using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerrarObjeto : MonoBehaviour
{
    public GameObject nextgameObject;
    public void CerrarPopUp()
    {
        this.gameObject.SetActive(false);
    }

    public void SiguientePaso()
    {
        nextgameObject.SetActive(true);
    }
}
