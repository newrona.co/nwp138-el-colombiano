using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ControlUI;
using UnityEngine.Video;
using UnityEngine.Networking;

public class VideosNoticias : MonoBehaviour
{
    private GameController gameController;
    private AudioSource audioController;
    public VideoPlayer videoEscenario;
    public Image botonPausa;
    public Image botonMute;
    public Sprite[] estadoBotonPlay;
    public Sprite[] estadoBotonMute;
    public string[] Url;

    private float volumen;

    private void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();
        audioController = FindObjectOfType<AudioSource>();
        volumen = audioController.volume;
        MostrarNoticia();
    }

    public void MostrarNoticia()
    {
        videoEscenario.url = System.IO.Path.Combine(Application.streamingAssetsPath, Url[0]);
        videoEscenario.prepareCompleted += PrepareTransicion;
        videoEscenario.Prepare();
    }
    void PrepareTransicion(VideoPlayer source)
    {
        videoEscenario.Play();
    }

    private void Awake()
    {
        if (videoEscenario.GetDirectAudioVolume(0) == 0)
        {
            botonMute.sprite = estadoBotonMute[1];
        }
        else
        {
            botonMute.sprite = estadoBotonMute[0];
        }

  

    }

    public void PlayPausa()
    {
        if (videoEscenario.isPlaying)
        {
            videoEscenario.Pause();
            botonPausa.sprite = estadoBotonPlay[0];
        }
        else 
        {
            videoEscenario.Play();
            botonPausa.sprite = estadoBotonPlay[1];
        }
        
    }
    public void MuteVideo()
    {
        if (videoEscenario.GetDirectAudioVolume(0) == 0)
        {
            botonMute.sprite = estadoBotonMute[0];
            videoEscenario.SetDirectAudioVolume(0, 0.5f);  
        }
        else {
            botonMute.sprite = estadoBotonMute[1];
            videoEscenario.SetDirectAudioVolume(0, 0);
        }    
    }

    public void ResetVideo() 
    {
        videoEscenario.Stop();
        videoEscenario.Play();
        botonPausa.sprite = estadoBotonPlay[1];
    }

    public void VolumenOn()
    {
        audioController.volume = volumen;
    }

}
