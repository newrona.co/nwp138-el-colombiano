using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ControlUI;

public class PreguntaPerfil: MonoBehaviour
{
    private GameController gameController;
    //Archivo Json en .txt
    public TextAsset textJson;
    private string jsonString;

    public int numeroPreguta;

    public TMP_Text text_pregunta;
    public TMP_Text text_respuesta_a;
    public TMP_Text text_respuesta_b;
    public TMP_Text text_respuesta_c;
    public TMP_Text text_respuesta_d;

    public int opcionEscojida = 0;

    public DataPreguntasList listaPreguntas = new DataPreguntasList();

    public GameObject sinRespuesta;
    public GameObject RespuestaCorrecta;
    public GameObject RespuestaIncorrecta;
    public Dialogos dialogos;
    private Image spriteLetra;

    public Button[] botonesOpciones;
    public Sprite spriteBotonesOn;
    public Sprite spriteBotonesOff;

    public PlayerController Pj;
    public Cuerpo cuerpo;

    private void Start()
    {
        LeerJson();
    }

    public void MostrarPregunta(int _numeroPregunta)
    {
        gameController = FindObjectOfType<GameController>();
        cuerpo.SeleccionarPlayer();
        numeroPreguta = _numeroPregunta;
        LeerJson();
        text_respuesta_a.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionA.texto;
        text_respuesta_b.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionB.texto;
        text_respuesta_c.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionC.texto;
        text_respuesta_d.text = listaPreguntas.preguntas[numeroPreguta - 1].opcionD.texto;
        text_pregunta.text = listaPreguntas.preguntas[numeroPreguta - 1].pregunta;
    }




    public void ContinuarGuion()
    {
        //EnviarPuntaje();
        dialogos.Paso();
    }
    public void SeleccionarOpcion(int _opcion)
    {
        opcionEscojida = _opcion;
        int count = 0;
        foreach (Button _boton in botonesOpciones)
        {   
            _boton.GetComponent<Image>().sprite = spriteBotonesOff;
            count++;
        }
        botonesOpciones[_opcion - 1].GetComponent<Image>().sprite = spriteBotonesOn;
    }

    public void Responder()
    {
        if (opcionEscojida == 0)
        { sinRespuesta.SetActive(true); }
        else 
        {
            int count = 0;
            foreach (Button _boton in botonesOpciones)
            {
                _boton.GetComponent<Image>().sprite = spriteBotonesOff;
                count++;
            }
            EnviarPuntaje();
            dialogos.Paso();
        }
        
    }

    public void EnviarPuntaje()
    {
        switch (opcionEscojida)
        {
            case 1:
                gameController.puntosInteractivo++;
                break;

            case 2:
                gameController.puntosBackend++;
                break;

            case 3:
                gameController.puntosFrontend++;
                break;

            case 4:
                gameController.puntosFullstack++;
                break;
        }
    }


    [System.Serializable]
    public class DataRespuetas
    {
        public string texto;
        public string inclinacion;
        public int multiplicador;
    }
   
    [System.Serializable]
    public class DataPreguntas
    {
        public int numero;
        public string pregunta;
        public bool directa;
        public DataRespuetas opcionA;
        public DataRespuetas opcionB;
        public DataRespuetas opcionC;
        public DataRespuetas opcionD;
    }
    [System.Serializable]
    public class DataPreguntasList
    {
        public DataPreguntas[] preguntas;
    }

    void LeerJson()
    {
         listaPreguntas = JsonUtility.FromJson<DataPreguntasList>(textJson.text);
    }
}
