using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ControlUI;

public class Caja_Seleccion : MonoBehaviour
{   
    public Playercustom player;
    public Button Boton_Atras;
    public Button Boton_Adelante;
    public GameObject Bloqueo;
    public GameObject nuevo;


    public Sprite[] sprites;

    public int id_prenda;
    public Enum_Partes parte;
    public Image item;

    private PlayerController pj;
    private GameController gameController;
    public int prendasDesbloqueadas = 0;
    public int prendasCabeza = 0;

    int maxItems;
    
    void Start() {
        gameController = FindObjectOfType<GameController>();
        pj = FindObjectOfType<PlayerController>();
        Boton_Atras.onClick.AddListener(ClickAtras);
        Boton_Adelante.onClick.AddListener(ClickAdelante);
        maxItems = sprites.Length - 1;    
    }
    public void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();
        pj = FindObjectOfType<PlayerController>();
        NumeroPrendas();
        updatePrenda();
    }

    void ClickAdelante()
    {
        id_prenda += 1;
        if (id_prenda > maxItems) 
        {
            id_prenda = 0;
            //Boton_Adelante.enabled = false;
        }
        item.sprite = sprites[id_prenda];
        nuevo.SetActive(false);
        updatePrenda();
        //Boton_Atras.enabled = true;

    }
    void ClickAtras()
    {
        id_prenda -= 1;
        if (id_prenda < 0) 
        {   
            id_prenda = maxItems;
            //Boton_Atras.enabled = false;
        }
        item.sprite = sprites[id_prenda];
        nuevo.SetActive(false);
        updatePrenda();
        //Boton_Adelante.enabled = true;
    }

    public bool ChechearPartes(Enum_Partes _partecuerpo, int id)
    {
        foreach (var _parte in gameController.PartesDesbloqueadas.PartesLista)
        {
            if ((_parte.parte == _partecuerpo.ToString()) && (_parte.id == id))
            {
                return _parte.desbloqueado;
            }
        }
        return false;
    }

    public void NumeroPrendas()
    {
        prendasCabeza = 0;
        foreach (var _parte in gameController.PartesDesbloqueadas.PartesLista)
        {
            //Debug.LogFormat("Parte : {0} || Desbloqueado : {1}", _parte.parte, _parte.desbloqueado );
            if ((_parte.parte == parte.ToString()) && (_parte.desbloqueado))
            {
                prendasCabeza++;
                //Debug.LogFormat("Numero de Partes : {0} ", prendasCabeza);
            }
        }
        
        if (prendasCabeza > prendasDesbloqueadas)
        {
            prendasDesbloqueadas = prendasCabeza;
            nuevo.SetActive(true);
        }
        else
        {
            nuevo.SetActive(false);
        }
    }

    void updatePrenda()
    {
        switch (parte)
        {
            case Enum_Partes.Cuerpo:
                if (ChechearPartes(Enum_Partes.Cuerpo, id_prenda))
                {
                    player.id_Cuerpo = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Cuerpo);
                }
                else { Bloqueo.SetActive(true); }
                
            break;

            case Enum_Partes.Cabeza:
                if (ChechearPartes(Enum_Partes.Cabeza, id_prenda))
                {
                    player.id_Cabeza = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Cabeza);
                }
                else { Bloqueo.SetActive(true); }
                break;

            case Enum_Partes.Camisa:
                if (ChechearPartes(Enum_Partes.Camisa, id_prenda))
                {
                    player.id_Camiza = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Camisa);
                }
                else { Bloqueo.SetActive(true); }
                break;

            case Enum_Partes.Pantalon:
                if (ChechearPartes(Enum_Partes.Pantalon, id_prenda))
                {
                    player.id_Pantalones = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Pantalon);
                }
                else { Bloqueo.SetActive(true); }
                break;

            case Enum_Partes.Zapatos:
                if (ChechearPartes(Enum_Partes.Zapatos, id_prenda))
                {
                    player.id_Zapatos = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Zapatos);
                }
                else { Bloqueo.SetActive(true); }
                break;

            case Enum_Partes.Accesorio:
                if (ChechearPartes(Enum_Partes.Accesorio, id_prenda))
                {
                    player.id_Accesorios = id_prenda;
                    Bloqueo.SetActive(false);
                    player.UpdatePersonaje(Enum_Partes.Accesorio);
                }
                else { Bloqueo.SetActive(true); }
                break;
        }
        //pj.partesPlayer.Add(parte, id_prenda);
        
    }
}
