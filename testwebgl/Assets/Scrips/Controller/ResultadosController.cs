using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ControlUI;
using System.Linq;
using TMPro;

public class ResultadosController : MonoBehaviour
{
    private GameController gameController;
    public GameObject bloqueo;
    public GameObject Resultado;
    public GameObject BotonInfo;
    public Puntuacion[] marcadores;
    public Image arana;
    public Sprite[] spritesArana;
    public Image textoPerfil;
    public Sprite[] spritesPerfil;

    public int puntuacionMayor;
    public int indexPerfil;
    private int porcentajeUbicaciones;


    public TMP_Text cantidadMapas;
    public TMP_Text cantidadNotas;
    public TMP_Text cantidadLineas;
    public TMP_Text cantidadPrendas;

    public TMP_Text texto;
    public string[] textosResultados;

    public string[] textoFullstack;
    public string[] textosBackend;
    public string[] textosFrontEnd;
    public string[] textosInteractivo;

    public Cuerpo cuerpo;

    private void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();

        if (!gameController.LineasCodigo.All(b => b == true))
        {
            bloqueo.SetActive(true);
        }
        else 
        {
            Resultado.SetActive(true);
            cuerpo.SeleccionarPlayer();
        }
        Fijarpuntuaciones();
        PuntuacionMayor();
        BotonInfo.SetActive(false);
    }

    private void Fijarpuntuaciones()
    {
        marcadores[0].SetPuntuacion(gameController.puntosInteractivo);
        marcadores[1].SetPuntuacion(gameController.puntosBackend);
        marcadores[2].SetPuntuacion(gameController.puntosFrontend);
        marcadores[3].SetPuntuacion(gameController.puntosFullstack);
    }

    private void PuntuacionMayor()
    {
        if (gameController.puntosInteractivo > puntuacionMayor)
        {
            puntuacionMayor = gameController.puntosInteractivo;
            indexPerfil = 0;
        }
        if (gameController.puntosBackend > puntuacionMayor)
        {
            puntuacionMayor = gameController.puntosBackend;
            indexPerfil = 1;
        }
        if (gameController.puntosFrontend > puntuacionMayor)
        {
            puntuacionMayor = gameController.puntosFrontend;
            indexPerfil = 2;
        }
        if (gameController.puntosFullstack > puntuacionMayor)
        {
            puntuacionMayor = gameController.puntosFullstack;
            indexPerfil = 3;
        }
        SelecionarAranayMensaje();
    }
    private void SelecionarAranayMensaje()
    {
        arana.sprite = spritesArana[indexPerfil];
        textoPerfil.sprite = spritesPerfil[indexPerfil];
        SetTextoPerfil();

        ActualizarNotas();
        ActualizarMapas();
        ActualizarLineas();
        ActualizarPrendas();
    }

    private void SetTextoPerfil()
    {
        switch (indexPerfil)
        {
            case 0:
                texto.text = textosInteractivo[Random.Range(0, 1)];
                break;
            case 1:
                texto.text = textosBackend[Random.Range(0, 1)];
                break;
            case 2:
                texto.text = textosFrontEnd [Random.Range(0, 1)];
                break;
            case 3:
                texto.text = textoFullstack [Random.Range(0, 1)];
                break;
        }
    }

    private void ActualizarNotas()
    {
        int cantidad = 0;
        foreach (bool _check in gameController.Notas)
        {
            if (_check)
            { cantidad++; }
        }
        cantidadNotas.text = cantidad.ToString();
    }

    private void ActualizarLineas()
    {
        int cantidad = 0;
        foreach (bool _check in gameController.LineasCodigo)
        {
            if (_check)
            { cantidad++; }
        }
        cantidadLineas.text = cantidad.ToString();
    }

    private void ActualizarMapas()
    {
        int count = 0;
        int cantidad = 0;
        porcentajeUbicaciones = 0;
        foreach (bool _check in gameController.EscenariosDesbloqueados)
        {
            if (_check)
            {
                cantidad++;
                if (cantidad <= 4)
                {
                    porcentajeUbicaciones += 15;
                }
                else
                {
                    porcentajeUbicaciones += 10;
                }
            }
            count++;
        }
        cantidadMapas.text = porcentajeUbicaciones.ToString();
    }


    private void ActualizarPrendas()
    {
        int cantidad = 0;
        int count = gameController.PartesDesbloqueadas.PartesLista.Length;
        //Debug.LogFormat("Numero de Prendas {0}", count);
        for (int i = 0; i < count; i++)
        {
            if (gameController.PartesDesbloqueadas.PartesLista[i].desbloqueado)
            {
                //Debug.LogFormat("id Prenda desbloqueadas {0}", i);
                cantidad++;
            }
        }
        cantidadPrendas.text = cantidad.ToString();
    }
}