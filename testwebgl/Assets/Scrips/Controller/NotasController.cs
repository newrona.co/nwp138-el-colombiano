using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ControlUI;

public class NotasController : MonoBehaviour
{
    private GameController gameControler;
    public MenuController menuController;
    public GameObject[] notas;
    public GameObject nuevaNota;


    private void OnEnable()
    {
        gameControler = FindObjectOfType<GameController>();
        int count = 0;
        int notasActivas = 0;
        foreach (bool _nota in gameControler.Notas)
        {
            notas[count].GetComponent<Nota>().Activo = _nota;
            notas[count].GetComponent<Nota>().ActivarNotas();
            count++;
            if (_nota)
            {
                notasActivas++;
            }
        }
        menuController.notasLeidas = notasActivas;
        nuevaNota.SetActive(false);
    }
}
