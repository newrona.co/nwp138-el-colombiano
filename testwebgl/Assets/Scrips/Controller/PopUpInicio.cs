using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using ControlUI;

public class PopUpInicio : MonoBehaviour
{
    public VideoPlayer videoEscenario;
    public Animator animCarga;
    public AudioSource sourceAudio;
    public GameController gameController;
    public string Url;

    private void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();
        animCarga.SetTrigger("fadeIn");
        MostrarTransicion();
    }


    #region Transicion
    public void MostrarTransicion()
    {
        videoEscenario.url = System.IO.Path.Combine(Application.streamingAssetsPath, Url);
        videoEscenario.prepareCompleted += PrepareTransicion;
        videoEscenario.Prepare();
    }
    void PrepareTransicion(VideoPlayer source)
    {
        videoEscenario.prepareCompleted -= PrepareTransicion;
        videoEscenario.Play();
        animCarga.SetTrigger("fadeOut");
        
    }

    public void AbrirMenu()
    {
        animCarga.SetTrigger("fadeIn");
        StartCoroutine(TiempoTransicion(3));
    }
    IEnumerator TiempoTransicion(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        gameController.UpdateNivel(Enum_Niveles.Menu);
    }
    #endregion
}
