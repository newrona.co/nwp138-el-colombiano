using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ControlUI;

public class LineasController : MonoBehaviour
{
    private GameController gameControler;
    public MenuController menuController;
    public GameObject nuevaLinea;
    public GameObject[] lineas;


    private void OnEnable()
    {
        gameControler = FindObjectOfType<GameController>();
        int count = 0;
        int lineasActivas = 0;
        foreach (bool _linea in gameControler.LineasCodigo)
        {
            lineas[count].GetComponent<Linea>().Activo = _linea;
            lineas[count].GetComponent<Linea>().ActivarLineas();
            count++;
            if (_linea )
            {
                lineasActivas++;
            }
        }
        menuController.lineasLeidas = lineasActivas;
        nuevaLinea.SetActive(false);

    }
}
