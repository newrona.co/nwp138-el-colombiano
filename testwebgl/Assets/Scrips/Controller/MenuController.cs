using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ControlUI;
using System.Linq;
using System;

public class MenuController : MonoBehaviour
{
    public GameController gameController;
    public PlayerController playerControler;


       
    [Header("Logros")]

    // Ubicaciones
    [Space]
    public Sprite[] estadoBotones;
    public Button[] botonesUbicaciones;
    private bool[] checkUbicaciones;
    private bool[] ubicacionesJugadas;
    public TMP_Text cantidadMapas;
    public TMP_Text cantidadMapasDescarga;
    public TMP_Text cantidadMapasFinal;
    public int porcentajeUbicaciones;

    // Notas de prensa
    [Space]
    private bool[] checkNotas;
    public TMP_Text cantidadNotas;
    public GameObject nuevaNota;
    public int notasLeidas;

    // Lineas del Codigo Parche
    [Space]
    private bool[] checkLineas;
    public TMP_Text cantidadLineas;
    public GameObject nuevaLinea;
    public int lineasLeidas;

    // Prendas y Accesorios
    [Space]
    private bool[] checkPrendas;
    public TMP_Text cantidadPrendas;
    public GameObject nuevaPrenda;
    public int prendasLeidas;

    [Space]
    public GameObject bloqueo;
    public GameObject resultados;



    public static Action<bool> OnMenuOpened;

    private void OnEnable()
    {
        ActualizarMenu();
    }

    [ContextMenu("Actulizar Menu")]
    public void ActualizarMenu()
    {
        ActualizarUbicaciones();
        ActualizarNotas();
        ActualizarLineas();
        ActualizarPrendas();
        OnMenuOpened?.Invoke(true);
    }

    private void ActualizarUbicaciones()
    {
        checkUbicaciones = gameController.EscenariosDesbloqueados;
        ubicacionesJugadas = gameController.EscenariosJugados;
        int count = 0;
        int cantidad = 0;
        porcentajeUbicaciones = 0;
        foreach (bool _check in checkUbicaciones)
        {   
            botonesUbicaciones[count].interactable = _check;
            botonesUbicaciones[count].GetComponent<Animator>().SetBool("Activo", _check);
            if (_check)
            {
                botonesUbicaciones[count].gameObject.GetComponentInChildren<Image>().sprite = estadoBotones[0];   
            }
            else
            {
                botonesUbicaciones[count].gameObject.GetComponentInChildren<Image>().sprite = estadoBotones[1];
            }
            if (ubicacionesJugadas[count])
            {
                botonesUbicaciones[count].gameObject.GetComponentInChildren<Image>().sprite = estadoBotones[2];
                cantidad++;
                if (cantidad <= 4)
                {
                    porcentajeUbicaciones += 15;
                }
                else
                {
                    porcentajeUbicaciones += 10;
                }
            }
            count ++;
        }
        
        cantidadMapas.text = porcentajeUbicaciones.ToString();
        cantidadMapasDescarga.text = string.Format("Has explorado el {0}% del Valle de Aburr�", porcentajeUbicaciones.ToString());
        cantidadMapasFinal.text = porcentajeUbicaciones.ToString();
    }
    
 

    private void ActualizarNotas()
    {   
        checkNotas = gameController.Notas;
        int cantidad = 0;
        foreach (bool _check in checkNotas)
        {
            if (_check)
            { cantidad++; }
        }
        cantidadNotas.text = cantidad.ToString();
        if (cantidad > notasLeidas)
        {
            nuevaNota.SetActive(true);
        }
    }

    private void ActualizarLineas()
    {
        checkNotas = gameController.LineasCodigo;
        int cantidad = 0;
        foreach (bool _check in checkNotas)
        {
            if (_check)
            { cantidad++; }
        }
        cantidadLineas.text = cantidad.ToString();
        if (cantidad > lineasLeidas)
        {
            nuevaLinea.SetActive(true);
        }  
    }

    private void ActualizarPrendas()
    {
        int cantidad = 0;
        int count = gameController.PartesDesbloqueadas.PartesLista.Length;
        //Debug.LogFormat("Numero de Prendas {0}", count);
        for (int i = 0; i < count; i++)
        {
            if (gameController.PartesDesbloqueadas.PartesLista[i].desbloqueado)
            {
                //Debug.LogFormat("id Prenda desbloqueadas {0}", i);
                cantidad++;
            }
        }
        cantidadPrendas.text = cantidad.ToString();
        if (cantidad > prendasLeidas)
        {
            nuevaPrenda.SetActive(true);
        }
    }


    public void FinalJuego()
    {
        gameController = FindObjectOfType<GameController>();

        if (!gameController.LineasCodigo.All(b => b == true))
        {
            gameController.EscenariosDesbloqueados[0] = true;
            ActualizarUbicaciones();
            bloqueo.SetActive(true);
        }
        else
        {
            resultados.SetActive(true);
        }
    }
}
