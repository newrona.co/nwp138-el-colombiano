using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EscenariosController : MonoBehaviour
{
    public UbicacionesController ubicaciones;
    public GameObject escenario;

    public int ubicacion;

    Enum_Escenarios[] enumValues = (Enum_Escenarios[])Enum.GetValues(typeof(Enum_Escenarios));
    public void SeleccionarUbicacion(int _ubicacion)
    {
        ubicacion = _ubicacion;
        CargarTransicion(_ubicacion);
        //ubicaciones.Escenario = enumValues[_ubicacion]; 
    }

    private void CargarTransicion(int _transicion)
    {
        switch (_transicion)
        {
            case 1:
                ubicaciones.Escenario = Enum_Escenarios.Metro;
                break;
            case 3:
                ubicaciones.Escenario = Enum_Escenarios.Moto;
                break;
            case 5:
                ubicaciones.Escenario = Enum_Escenarios.Bus;
                break;
            case 6:
                ubicaciones.Escenario = Enum_Escenarios.Metro;
                break;
            case 7:
                ubicaciones.Escenario = Enum_Escenarios.Moto;
                break;
            case 8:
                ubicaciones.Escenario = Enum_Escenarios.Metro;
                break;
            case 9:
                ubicaciones.Escenario = Enum_Escenarios.Metro;
                break;
            case 10:
                ubicaciones.Escenario = Enum_Escenarios.Bus;
                break;
            case 11:
                ubicaciones.Escenario = Enum_Escenarios.Metro;
                break;
        }
        ubicaciones.gameObject.SetActive(true);
        escenario.SetActive(true);
        StartCoroutine(TiempoTransicion(4f));
    }

    IEnumerator TiempoTransicion(float _delay)
    {
        yield return new WaitForSeconds(_delay );
        ubicaciones.Escenario = enumValues[ubicacion];
        ubicaciones.CambiarUbicacion();

    }
}
