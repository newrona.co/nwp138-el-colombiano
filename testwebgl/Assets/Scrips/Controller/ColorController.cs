using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ControlUI;
public class ColorController : MonoBehaviour
{
    private GameController gameController;
    public MenuController menuController;
    public GameObject nuevaPrenda;
    public Cuerpo Player;
    public Toggle[] toggleColores; 

    private void OnEnable()
    {
        gameController = FindObjectOfType<GameController>();
        ActualizarLogrosPrendas();
        ActualizarColores();
    }

    private void ActualizarColores()
    {
        foreach (var _color in gameController.PartesDesbloqueadas.PartesLista)
        {
            if ((_color.parte == Enum_Partes.Cuerpo.ToString()) && (_color.desbloqueado))
            {
                toggleColores[_color.id].interactable = true;
            }
        }
    }

    public void RegresarMenu()
    {
        Player.SeleccionarPlayer();
        this.gameObject.SetActive(false);
    }

    public void ActualizarLogrosPrendas()
    {
        int notasActivas = 0;
        int cantidad = 0;
        int count = gameController.PartesDesbloqueadas.PartesLista.Length;
        for (int i = 0; i > count; i++)
        {
            if (gameController.PartesDesbloqueadas.PartesLista[i].desbloqueado)
            {
                cantidad++;
            }
        }
        menuController.notasLeidas = notasActivas;
        nuevaPrenda.SetActive(false);
    }
}
