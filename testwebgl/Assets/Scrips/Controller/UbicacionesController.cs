using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Networking;

public class UbicacionesController : MonoBehaviour
{
    public Enum_Escenarios Escenario;
    public Enum_Escenarios Transicion;

    public VideoPlayer videoEscenario;
    public Animator animCarga;
    public AudioSource sourceAudio;
    public string[] Url;
    public AudioClip[] clips;

    public GameObject popUpEscenario;
    public GameObject BotonPopup;
    public GameObject BotonContinuar;
    public GameObject BotonAtras;
    public GameObject BotonInfo;
    private PopUpEscenario scr_popUpEscenario;

    public Dictionary<Enum_Escenarios, string> urlEscenarios;
    public Dictionary<Enum_Escenarios, AudioClip> clipsEscenarios;

    public Vector2[] posPopUp;
    private Dictionary<Enum_Escenarios, Vector2> map_posicion;

    public Dialogos guion;

    private void OnEnable()
    {
        scr_popUpEscenario = popUpEscenario.GetComponent<PopUpEscenario>();
        map_posicion = new Dictionary<Enum_Escenarios, Vector2>();
        UpdateUrl();
        MostrarTransicion();
    }


    public void CambiarUbicacion()
    {
        scr_popUpEscenario = popUpEscenario.GetComponent<PopUpEscenario>();
        map_posicion = new Dictionary<Enum_Escenarios, Vector2>();
        AsignarPosicion();
        UpdateUrl();
    }


    //Carga las urls de los escenarios
    private void UpdateUrl()
    {
        urlEscenarios = new Dictionary<Enum_Escenarios, string>();
        clipsEscenarios = new Dictionary<Enum_Escenarios, AudioClip>();
        animCarga.SetTrigger("fadeIn");
        int count = 0;
        foreach (Enum_Escenarios _Escenarios in Enum_Escenarios.GetValues(typeof(Enum_Escenarios)))
        {
            urlEscenarios.Add(_Escenarios, Url[count]);
            clipsEscenarios.Add(_Escenarios, clips[count]);
            count++;
        }
    }


    #region Transicion
    public void MostrarTransicion()
    {
        BotonContinuar.SetActive(false);
        BotonAtras.SetActive(false);
        BotonInfo.SetActive(false);
        videoEscenario.url = System.IO.Path.Combine(Application.streamingAssetsPath, urlEscenarios[Transicion]);
        videoEscenario.prepareCompleted += PrepareTransicion;
        videoEscenario.Prepare();
    }
    void PrepareTransicion(VideoPlayer source)
    {
        videoEscenario.Play();
        animCarga.SetTrigger("fadeOut");
        StartCoroutine(TiempoTransicion(3));
    }
    IEnumerator TiempoTransicion(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        videoEscenario.prepareCompleted -= PrepareTransicion;
        animCarga.SetTrigger("fadeIn");
        StartCoroutine(TiempoEscenario(1));
    }
    IEnumerator TiempoEscenario(float _delay)
    {
        yield return new WaitForSeconds(_delay);
        CambiarEscenario(Escenario);
    }
    #endregion




    #region Escenario
    public void CambiarEscenario(Enum_Escenarios _escenario)
    {
        //scr_popUpEscenario = popUpEscenario.GetComponent<PopUpEscenario>();
        //AsignarPosicion();
        Escenario = _escenario;
        AsignarPosicion();
        scr_popUpEscenario.UpdatePopup(_escenario);
        BotonContinuar.SetActive(false);
        BotonInfo.SetActive(true);
        BotonAtras.SetActive(true);
        guion.gameObject.SetActive(true);
        guion.IniciarGuion();
        
        if (map_posicion.ContainsKey(Escenario))
        {
           //BotonContinuar.SetActive(true);
            //BotonAtras.SetActive(true);
           // BotonPopup.gameObject.transform.localPosition = map_posicion[Escenario];
        }

        videoEscenario.url = System.IO.Path.Combine(Application.streamingAssetsPath, urlEscenarios[_escenario]);
        videoEscenario.prepareCompleted += PrepareCompleted;
        videoEscenario.Prepare();
    }
    void PrepareCompleted(VideoPlayer source)
    {
        videoEscenario.prepareCompleted -= PrepareCompleted;
        videoEscenario.Play();
        animCarga.SetTrigger("fadeOut");
        if (sourceAudio.clip != clipsEscenarios[Escenario])
        {
            sourceAudio.clip = clipsEscenarios[Escenario];
            sourceAudio.Play();
        }
        //BotonContinuar.SetActive(true);
        //BotonAtras.SetActive(true);
    }

    #endregion



    //Canbiar posicion del boton del PopUP dependiento del escnario
    public void AsignarPosicion()
    {
        map_posicion.Add(Enum_Escenarios.Cable, posPopUp[0]);
        map_posicion.Add(Enum_Escenarios.Castilla, posPopUp[1]);
        map_posicion.Add(Enum_Escenarios.Manrique, posPopUp[2]);
        map_posicion.Add(Enum_Escenarios.Periodistas, posPopUp[3]);
        map_posicion.Add(Enum_Escenarios.Ayacucho, posPopUp[4]);
        map_posicion.Add(Enum_Escenarios.Restrepo, posPopUp[5]);
        map_posicion.Add(Enum_Escenarios.Comuna, posPopUp[6]);
        map_posicion.Add(Enum_Escenarios.Cerro, posPopUp[7]);
        map_posicion.Add(Enum_Escenarios.Debora, posPopUp[8]);
        if (map_posicion.ContainsKey(Escenario))
        {
            //Debug.Log(map_posicion[Escenario]);
            BotonPopup.gameObject.transform.position = map_posicion[Escenario];
        }
    }


    /*
    IEnumerator DownLoadAsset(string _escenario)
    {
        string path = "StreamingAssets/" + _escenario;
        UnityWebRequest uwr = UnityWebRequest.Get(path);
        yield return uwr.SendWebRequest();
        videoEscenario.prepareCompleted += PrepareCompleted;
        videoEscenario.Prepare();
        //videoEscenario.Source(uwr.downloadHandler.data);
    }*/
}