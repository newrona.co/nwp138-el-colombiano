using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


namespace ControlUI
{
    public class GameController : MonoBehaviour
    {
        public static Enum_Niveles nivel;
        public static Dictionary<Enum_Niveles, GameObject> map_niveles;

        public GameObject[] GO_niveles;
        public GameObject[] GO_Escenarios;

        public Enum_Niveles degub_iniciar = Enum_Niveles.Intro;

        public bool[] LineasCodigo;
        public bool[] Notas;
        public bool[] EscenariosDesbloqueados;
        public bool[] EscenariosJugados;


        public TextAsset textCSV;
        public PartesList PartesDesbloqueadas;

        public int puntosInteractivo;
        public int puntosBackend;
        public int puntosFrontend;
        public int puntosFullstack;

        void Start()
        {
            //PartesDesbloqueadas = new PartesList();
            map_niveles = new Dictionary<Enum_Niveles, GameObject>();
            map_niveles.Add(Enum_Niveles.Intro, GO_niveles[0]);
            map_niveles.Add(Enum_Niveles.Menu, GO_niveles[1]);
            map_niveles.Add(Enum_Niveles.Fin, GO_niveles[2]);
            nivel = degub_iniciar;
            UpdateNivel(nivel);
        }


        public static bool ToBool(string aText)
        {
            return aText == "true" || aText == "on" || aText == "yes";
        }
        public void UpdateNivel(Enum_Niveles nuevoNivel)
        {
            DesactivarEscenarios();
            foreach (KeyValuePair<Enum_Niveles, GameObject> pair in map_niveles)
            {
                Enum_Niveles lvl = pair.Key;
                GameObject go_nivel = pair.Value;
                if (nuevoNivel == lvl)
                {
                    go_nivel.SetActive(true);
                }
                else {
                    go_nivel.SetActive(false);
                }
            }
        }

        private void DesactivarEscenarios()
        {
            foreach (GameObject _escenario in GO_Escenarios)
            {
                _escenario.SetActive(false);
            }
        }

        [System.Serializable]
        public class Partes
        {
            public string parte;
            public int id;
            public bool desbloqueado;
        }
        [System.Serializable]
        public class PartesList
        {
            public Partes[] PartesLista;
        }

    }
}

