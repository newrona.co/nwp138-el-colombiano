using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Enum_Partes partes;

    [SerializeField]
    public Dictionary<Enum_Partes, int> partesPlayer;

    public Dictionary<Enum_Partes, Sprite> partesSprite;
    public Color colorCuerpo;


    public int fullstack;
    public int backend;
    public int frontend;
    public int interactivo;

    public string Nombre;
    public int numeroUbicaciones;
    public int numeroNotas;
    public int numeroLineas;
    public List<int> notas;
    public List<string> Lineas;

    void Start()
    {
        partesPlayer = new Dictionary<Enum_Partes, int>();
        partesSprite = new Dictionary<Enum_Partes, Sprite>();
    }

    public void SelecionarSprite(Enum_Partes _parte, Sprite _sprite)
    {
        //partesSprite.Add(_parte, _sprite);
        partesSprite[_parte] =  _sprite;
    }
    public void SelecionarColor(Color _color)
    {
        colorCuerpo = _color;
    }

    public void SelecionarNombre(string _nombre)
    {
        Nombre = _nombre;
    }


    public void SumarPuntos(string _opcion, int _multiplicador)
    {
        switch (_opcion)
        {
            case "Interactivo":
                interactivo = interactivo + _multiplicador;
                break;
            case "Backend":
                backend = backend + _multiplicador;
                break;
            case "Frontend":
                frontend = frontend + _multiplicador;
                break;
            case "Fullstack":
                fullstack = fullstack + _multiplicador;
                break;
        }
    }
}
