using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Puntuacion : MonoBehaviour
{
    public int puntuacion;
    public GameObject[] puntos;

    public void SetPuntuacion(int _puntuacion)
    {
        puntuacion = _puntuacion;
        if (puntuacion > 0)
        {
            switch (_puntuacion)
            {
                case 1:
                    puntos[0].SetActive(true);
                    break;
                case 2:
                    puntos[0].SetActive(true);
                    puntos[1].SetActive(true);
                    break;
                case 3:
                    puntos[0].SetActive(true);
                    puntos[1].SetActive(true);
                    puntos[2].SetActive(true);
                    break;
                case 4:
                    puntos[0].SetActive(true);
                    puntos[1].SetActive(true);
                    puntos[2].SetActive(true);
                    puntos[3].SetActive(true);
                    break;
                case 5:
                    puntos[0].SetActive(true);
                    puntos[1].SetActive(true);
                    puntos[2].SetActive(true);
                    puntos[3].SetActive(true);
                    puntos[4].SetActive(true);
                    break;
            }
        }
        
    }
}
