using ControlUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [SerializeField] GameObject[] MainTutorialObjects;
    [SerializeField] Button nextButton;
    [SerializeField] Button skipButton;
    [SerializeField] Button backButton;
    [SerializeField] GameObject[] SecondPartObjects;
    [SerializeField] GameObject[] SecondPartInfos;
    [SerializeField] GameObject[] FourthPartInfos;

    public int state;

    private void OnEnable() => Init();

    private void Init()
    {
        skipButton.onClick?.RemoveAllListeners();
        skipButton.onClick.AddListener(SkipTutorial);
        backButton.gameObject.SetActive(false);
        LockTutorialMainScreen();
        ResetTutorial();
    }
    private void ResetTutorial()
    {
        state = 0;
        nextButton.onClick?.RemoveAllListeners();
        nextButton.onClick.AddListener(NexTutorialState);
        backButton.onClick?.RemoveAllListeners();
        backButton.onClick.AddListener(BackTutorialState);

        foreach (var item in MainTutorialObjects) item.SetActive(false);
        foreach (var item in SecondPartObjects) item.SetActive(true);
        foreach (var item in SecondPartInfos) item.SetActive(false);
        foreach (var item in FourthPartInfos) item.SetActive(false);

        MainTutorialObjects[0].SetActive(true);
        SecondPartInfos[0].SetActive(true);
        FourthPartInfos[0].SetActive(true);
    }

    public void BackTutorialState()
    {
        Debug.Log("backpress");
        state--;
        ChangeState();
    }


    public void NexTutorialState()
    {
        state++;
        ChangeState();
    }

    public void ChangeState()
    {
        switch (state)
        {
            case 0: Init(); break;
            case 1: ShowCustomPlayerScreen(); break;
            case 2: ShowCharacter(); break;
            case 3: ShowCharacterClothes(); break;
            case 4: ShowCharacterColors(); break;
            case 5: ShowMap(); break;
            case 6: ShowAchievements(); break;
            case 7: ShowExtraInfoAchievements(); break;
            case 8: ShowQuestion(); break;
            case 9: ShowMenu(); break;
            default: SkipTutorial(); break;
        }
    }
    private void SkipTutorial()
    {
        ResetTutorial();
        this.gameObject.SetActive(false);

    }
    private void ShowCustomPlayerScreen()
    {
        foreach (var item in SecondPartObjects)
        {
            item.gameObject.SetActive(true);
        }
        MainTutorialObjects[0].SetActive(false);
        MainTutorialObjects[1].SetActive(true);
        backButton.gameObject.SetActive(true);
        SecondPartInfos[0].SetActive(true);
        SecondPartInfos[1].SetActive(false);

    }
    private void ShowCharacter()
    {
        foreach (var item in SecondPartObjects)
        {
            item.gameObject.SetActive(false);
        }
        SecondPartObjects[0].SetActive(true);
        SecondPartObjects[1].SetActive(false);
        SecondPartInfos[0].SetActive(false);
        SecondPartInfos[1].SetActive(true);
        SecondPartInfos[2].SetActive(false);
    }
    private void ShowCharacterClothes()
    {
        SecondPartObjects[1].SetActive(true);
        SecondPartObjects[2].SetActive(false);
        SecondPartInfos[3].SetActive(false);
        SecondPartInfos[2].SetActive(true);
        SecondPartInfos[1].SetActive(false);
    }

    private void ShowCharacterColors()
    {
        MainTutorialObjects[1].SetActive(true);
        MainTutorialObjects[2].SetActive(false);
        SecondPartObjects[2].SetActive(true);
        SecondPartInfos[3].SetActive(true);
        SecondPartInfos[2].SetActive(false);
    }

    private void ShowMap()
    {
        MainTutorialObjects[1].SetActive(false);
        MainTutorialObjects[2].SetActive(true);
        MainTutorialObjects[3].SetActive(false);
    }

    private void ShowAchievements()
    {
        MainTutorialObjects[2].SetActive(false);
        MainTutorialObjects[3].SetActive(true);
        FourthPartInfos[0].SetActive(true);
        FourthPartInfos[1].SetActive(false);
    }

    private void ShowExtraInfoAchievements()
    {
        MainTutorialObjects[2].SetActive(false);
        MainTutorialObjects[3].SetActive(true);
        FourthPartInfos[0].SetActive(false);
        FourthPartInfos[1].SetActive(true);
        MainTutorialObjects[4].SetActive(false);
    }

    private void ShowQuestion()
    {
        MainTutorialObjects[3].SetActive(false);
        MainTutorialObjects[4].SetActive(true);
        MainTutorialObjects[5].SetActive(false);

    }

    private void ShowMenu()
    {
        Debug.Log("Show Menu");
        MainTutorialObjects[4].SetActive(false);
        MainTutorialObjects[5].SetActive(true);

    }

    private void LockTutorialMainScreen()
    {
        if(FindObjectOfType<PopNombre>()?.inputNombre.text == "")
            gameObject.SetActive(false);
    }




}
