using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopUpEscenario : MonoBehaviour
{
    public Enum_Escenarios escenario;
    public Color[] colores;
    private Enum_Colores color;
    private Dictionary<Enum_Escenarios, Enum_Colores> map_colores;
    private Dictionary<Enum_Colores, Color> map_color;
    public Image marco;

    public TMP_Text titulo;
    public TMP_Text descripcion;


    // Start is called before the first frame update
    void Start()
    {
        map_colores = new Dictionary<Enum_Escenarios, Enum_Colores>();
        map_color = new Dictionary<Enum_Colores, Color>();
        AsignarColor();
        LeerJson();


    }

    public void UpdatePopup(Enum_Escenarios _Escenario)
    {
        escenario = _Escenario;
        //marco.color = (map_color[map_colores[escenario]]);
        for (int i = 0; i < todosEscanarios.escenarios.Length; i++)
        {
            if (todosEscanarios.escenarios[i].id == escenario.ToString())
            {
                titulo.text = todosEscanarios.escenarios[i].titulo;
                descripcion.text = todosEscanarios.escenarios[i].descripcion;
                break;
            }
        }
    }


    //Asigna a cada escenario un color para el PopUp
    public void AsignarColor()
    {
        map_colores.Add(Enum_Escenarios.Metro, Enum_Colores.Rojo);
        map_colores.Add(Enum_Escenarios.Moto, Enum_Colores.Rojo);
        map_colores.Add(Enum_Escenarios.Bus, Enum_Colores.Rojo);
        map_colores.Add(Enum_Escenarios.Cable, Enum_Colores.Verde);
        map_colores.Add(Enum_Escenarios.Castilla, Enum_Colores.Azul);
        map_colores.Add(Enum_Escenarios.Ayacucho, Enum_Colores.Lila);
        map_colores.Add(Enum_Escenarios.Debora, Enum_Colores.Azul);
        map_colores.Add(Enum_Escenarios.Periodistas, Enum_Colores.Purpura);
        map_colores.Add(Enum_Escenarios.Manrique, Enum_Colores.Amarillo);
        map_colores.Add(Enum_Escenarios.Cerro, Enum_Colores.Morado);
        map_colores.Add(Enum_Escenarios.Comuna, Enum_Colores.Amarillo);
        map_colores.Add(Enum_Escenarios.Restrepo, Enum_Colores.Lila);

        map_color.Add(Enum_Colores.Verde, colores[0]);
        map_color.Add(Enum_Colores.Lila, colores[1]);
        map_color.Add(Enum_Colores.Azul, colores[2]);
        map_color.Add(Enum_Colores.Amarillo, colores[3]);
        map_color.Add(Enum_Colores.Morado, colores[4]);
        map_color.Add(Enum_Colores.Purpura, colores[5]);
        map_color.Add(Enum_Colores.Rojo, colores[6]);

        marco.color = (map_color[map_colores[escenario]]);
    }


    #region TextJson
        
        //Archivo Json en .txt
        public TextAsset textJson;
        private string jsonString;

        public DataList todosEscanarios = new DataList();

        [System.Serializable]
        public class Data
        {
            public string id;
            public string titulo;
            public string descripcion;
        }
        [System.Serializable]
        public class DataList
        {
            public Data[] escenarios;
        }


        void LeerJson()
        {
            todosEscanarios = JsonUtility.FromJson<DataList>(textJson.text);

            for (int i = 0; i < todosEscanarios.escenarios.Length; i++)
            {
                if (todosEscanarios.escenarios[i].id == escenario.ToString())
                {
                    titulo.text = todosEscanarios.escenarios[i].titulo;
                    descripcion.text = todosEscanarios.escenarios[i].descripcion;
                    break;
                }
            }
            //todosEscanarios.escenarios[0].id
            //Debug.Log(todosEscanarios.escenarios.Length);
        }

    #endregion

}
