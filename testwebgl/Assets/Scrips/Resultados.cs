using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Resultados : MonoBehaviour
{
    public PlayerController Pj;

    public Sprite[] marcosDefinicion;
    public Image marcoDefinicion;

    public Sprite[] telaranas;
    public Image telarana;

    public string[] textosResultado;
    public TMP_Text testoResultao;

    public MedidorPuntos[] medidores;

    private int[] puntos;
    private int indexGanador;
    private int puntuacionMayor;

    public void CalcularResultados()
    {
        puntos = new int[] { Pj.interactivo, Pj.backend, Pj.frontend, Pj.fullstack };
        puntuacionMayor = Mathf.Max(Pj.interactivo, Pj.backend, Pj.frontend, Pj.fullstack);
        indexGanador = System.Array.IndexOf(puntos, puntuacionMayor);

        marcoDefinicion.sprite = marcosDefinicion[indexGanador];
        telarana.sprite = telaranas[indexGanador];
        testoResultao.text = textosResultado[indexGanador];

        medidores[0].Calcular(Pj.interactivo);
        medidores[1].Calcular(Pj.backend);
        medidores[2].Calcular(Pj.frontend);
        medidores[3].Calcular(Pj.fullstack);
    }
}
