using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Linea : MonoBehaviour
{

    public bool Activo;
    public int idLinea;

    public string textoNota;
    public GameObject Bloqueo;
    public GameObject Abierto;


    private void OnEnable()
    {
        ActivarLineas();
    }
    public void ActivarLineas() 
    {
        if (Activo)
        {
            Bloqueo.SetActive(false);
            Abierto.SetActive(true);
        }
        else
        {
            Bloqueo.SetActive(true);
            Abierto.SetActive(false);
        }
    }

}
