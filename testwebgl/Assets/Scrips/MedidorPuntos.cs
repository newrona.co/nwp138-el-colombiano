using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedidorPuntos : MonoBehaviour
{
    public GameObject[] Fulls;

    public Color color;

    public int puntosMax;
    public int puntos;

    public void Calcular(int _puntos)
    {
        puntos = _puntos;
        foreach (GameObject _full in Fulls)
        {
            _full.GetComponent<Image>().color = color;
        }

        int x = (puntos * 100) / puntosMax;
        if (x > 80)
        { 
            Fulls[0].SetActive(true);
            Fulls[1].SetActive(true);
            Fulls[2].SetActive(true);
            Fulls[3].SetActive(true);
            Fulls[4].SetActive(true);
        }
        if (x <= 80)
        {
            Fulls[0].SetActive(true);
            Fulls[1].SetActive(true);
            Fulls[2].SetActive(true);
            Fulls[3].SetActive(true);
            Fulls[4].SetActive(false);
        }
        if (x <= 60)
        {
            Fulls[0].SetActive(true);
            Fulls[1].SetActive(true);   
            Fulls[2].SetActive(true);
            Fulls[3].SetActive(false);
            Fulls[4].SetActive(false);
        }
        if (x <= 40)
        {
            Fulls[0].SetActive(true);
            Fulls[1].SetActive(true);
            Fulls[2].SetActive(false);
            Fulls[3].SetActive(false);
            Fulls[4].SetActive(false);
        }
        if (x <= 20)
        {
            Fulls[0].SetActive(true);
            Fulls[1].SetActive(false);
            Fulls[2].SetActive(false);
            Fulls[3].SetActive(false);
            Fulls[4].SetActive(false);
        }

    }
}
