using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Nota : MonoBehaviour
{

    public bool Activo;
    public int idNota;

    public string textoNota;
    public GameObject Bloqueo;
    public GameObject Abierto;
    public GameObject notaPopup;
    public TMP_Text texto;

    private void OnEnable()
    {
        ActivarNotas();
    }
    public void ActivarNotas() 
    {
        if (Activo)
        {
            Bloqueo.SetActive(false);
            Abierto.SetActive(true);
        }
        else
        {
            Bloqueo.SetActive(true);
            Abierto.SetActive(false);
        }
    }


    public void MostrarPopup()
    {
        texto.text = textoNota;
        notaPopup.SetActive(true);
    }
    public void CerrarPopup()
    {
        notaPopup.SetActive(false);
    }
}
