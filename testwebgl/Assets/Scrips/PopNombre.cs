using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


namespace ControlUI
{

    public class PopNombre : MonoBehaviour
    {
        public PlayerController pj;
        public TMP_InputField inputNombre;
        public GameController gameController;
        public GameObject sinNombre;
        public GameObject popInicio;
        public GameObject tutorial;

        public void ComprobarNombre()
        {
            if (inputNombre.text != "")
            {
                pj.SelecionarNombre(inputNombre.text);
                //popInicio.SetActive(true);
                gameController.UpdateNivel(Enum_Niveles.Menu);
                //this.gameObject.GetComponent<Animator>().SetTrigger("Cerrar");
                tutorial.SetActive(true);
            }
            else {
                sinNombre.SetActive(true);
            }
        }
    }
}


