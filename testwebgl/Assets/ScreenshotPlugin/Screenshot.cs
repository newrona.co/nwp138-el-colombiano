using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class Screenshot : MonoBehaviour
{

#if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    private static extern void takeScreenShot();
#endif

    public GameObject mensajeDescarga;

    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(TakeScreenshot);
    }
    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(TakeScreenshot);
    }

    public void TakeScreenshot()
    {
        StartCoroutine(GetImage());
    }

    IEnumerator GetImage()
    {
        Debug.Log("Getting Image");
        yield return new WaitForEndOfFrame();
#if UNITY_WEBGL && !UNITY_EDITOR
        takeScreenShot();
#endif
        mensajeDescarga.SetActive(true);
    }
}
